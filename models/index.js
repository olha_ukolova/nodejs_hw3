const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.user = require('./user.model');

db.load = require('./load.model');

db.truck = require('./trucks.model');

module.exports = db;

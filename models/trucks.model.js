const mongoose = require('mongoose');

const Truck = mongoose.model(
    'Truck',
    new mongoose.Schema({
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
      },
      type: {
        type: String,
      },
      status: {
        type: String,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
    ),
);

module.exports = Truck;

const mongoose = require('mongoose');

const User = mongoose.model(
    'User',
    new mongoose.Schema({
      email: {
        type: String,
        unique: true,
        required: true,
      },
      password: {
        type: String,
        required: true,
      },
      role: {
        type: String,
      },
      created_date: {
        type: Date,
        default: Date.now(),
      },
    } ),
);

module.exports = User;

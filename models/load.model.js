const mongoose = require('mongoose');

const Load = mongoose.model(
    'Load',
    new mongoose.Schema({
      created_by: {
        type: String,
      },
      assigned_to: {
        type: String,
      },
      status: {
        type: String,
      },
      state: {
        type: String,
      },
      name: {
        type: String,
      },
      payload: {
        type: Number,
      },
      delivery_address: {
        type: String,
      },
      pickup_address: {
        type: String,
      },
      dimensions: {
        type: Object,
        of: Number,
      },
      logs: [{
        message: String,
        time: {
          type: Date,
          default: Date.now(),
        },
      }],
      created_date: {
        type: Date,
        default: Date.now(),
      },
    },
    ),
);

module.exports = Load;

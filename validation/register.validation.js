const Joi = require('joi');

const registerValidation = Joi.object({

  email: Joi.string()
      .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}}),

  password: Joi.string().min(5).required(),

  role: Joi.string().valid('DRIVER', 'SHIPPER').required(),

  created_date: Joi.any(),

  _id: Joi.any(),
});

module.exports = registerValidation;

const Joi = require('joi');


const loadValidation = Joi.object({
  _id: Joi.any(),
  created_by: Joi.string()
      .required(),

  assigned_to: Joi.string()
      .required(),

  status: Joi.string()
      .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED')
      .required(),

  state: Joi.string()
      .valid( '',
          'En route to pick up',
          'Arrived to Pick Up',
          'En route to delivery',
          'Arrived to delivery')
      .required(),
  name: Joi.string()
      .required(),
  delivery_address: Joi.string()
      .required(),
  pickup_address: Joi.string()
      .required(),

  dimensions: Joi.object().min(3)
      .required(),

  payload: Joi.number()
      .required(),

  logs: Joi.any(),

  created_date: Joi.any(),

  __v: Joi.number(),
});

module.exports = loadValidation;

const controller = require('../controllers/trucks.controller');

const {onlyDriver} = require('../middlewares');

const express = require('express');

const router = express.Router();

router.post('/', onlyDriver, controller.createTruck);

router.get('/', onlyDriver, controller.getTrucks);

router.get('/:id', onlyDriver, controller.getTruckById);

router.put('/:id', onlyDriver, controller.updateTruckById);

router.delete('/:id', onlyDriver, controller.deleteTruckById);

router.post('/:id/assign', onlyDriver, controller.assignTruckById);

module.exports = router;

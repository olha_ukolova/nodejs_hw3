const controller = require('../controllers/loads.controller');
const {onlyShipper} = require('../middlewares');
const {onlyDriver} = require('../middlewares');

const express = require('express');

const router = express.Router();

router.get('/', controller.getLoads);

router.post('/', onlyShipper, controller.createLoad);

router.patch('/active/state', onlyDriver, controller.changeState);

router.get('/active', onlyDriver, controller.getActive);

router.get('/:id', controller.getLoadById);

router.put('/:id', onlyShipper, controller.updateLoadById);

router.delete('/:id', onlyShipper, controller.deleteLoadById);

router.post('/:id/post', onlyShipper, controller.postLoadById,
    controller.postSystemLoadById);

router.get('/:id/shipping_info', onlyShipper, controller.shippingInfo);

module.exports = router;

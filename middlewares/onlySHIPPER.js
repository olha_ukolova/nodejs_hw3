const db = require('../models');

onlyShipper = async (req, res, next) => {
  db.user.findById({_id: req.userId}, (error, data) => {
    if (error||!data) {
      res.status(400).send({
        'message': 'Can`t find user info',
      });
    } else if (data.role === 'DRIVER') {
      res.status(400).send({
        'message': 'DRIVER can`t do it!',
      });
    } else {
      next();
    }
  });
};

module.exports = onlyShipper;

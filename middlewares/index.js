const authJwt = require('./authJwt');
const onlyDriver = require('./onlyDRIVER');
const onlyShipper = require('./onlySHIPPER');


module.exports = {
  authJwt,
  onlyShipper,
  onlyDriver,
};

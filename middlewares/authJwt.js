require('dotenv').config({path: '../.env'});
const jwt = require('jsonwebtoken');

const config = process.env.SECRET;


verifyToken = (req, res, next) => {
  const bearerToken = req.headers.authorization.split(' ')[1]?
   req.headers.authorization.split(' ')[1]:
    req.headers.authorization;

  if (!bearerToken) {
    return res.status(400).send({message: 'No token provided!'});
  }

  jwt.verify(bearerToken, config, (err, decoded) => {
    if (err) {
      return res.status(400).send({message: 'Unauthorized!'});
    }
    req.userId = decoded._id;
    req.email = decoded.email;
    next();
  });
};

const authJwt = verifyToken;

module.exports = authJwt;

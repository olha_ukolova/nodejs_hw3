const db = require('../models');

onlyDriver = async (req, res, next) => {
  db.user.findById({_id: req.userId}, (error, data) => {
    if (error||!data) {
      res.status(400).send({
        'message': 'Can`t find user info',
      });
    } else if (data.role === 'SHIPPER') {
      res.status(400).send({
        'message': 'SHIPPER can`t work with truck!',
      });
    } else {
      next();
    }
  });
};

module.exports = onlyDriver;

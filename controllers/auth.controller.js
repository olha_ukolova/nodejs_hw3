require('dotenv').config({path: '../.env'});
const config = process.env.SECRET;
const db = require('../models');
const User = db.user;

const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const registerValidation = require('../validation/register.validation');
const loginValidation = require('../validation/login.validation');

exports.register = async (req, res) => {
  const user = new User({
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8),
    role: req.body.role,
  });
  const userCheck = await User.findOne({email: req.body.email});
  if ( userCheck) {
    return res.status(400).send({
      'message': 'This email exist',
    });
  }
  try {
    await registerValidation.validateAsync(user._doc);
    await user.save();
    res.status(200).send({
      'message': 'User registered',
    });
  } catch (err) {
    res.status(400).send({
      'message': 'Invalid validation',
    });
  }
};

exports.login = async (req, res) => {
  try {
    const userdIsValid = {
      email: req.body.email,
      password: req.body.password,
    };

    const user = await User.findOne({email: req.body.email});
    if ( !user ) {
      return res.status(400).send({
        'message': 'Cannot find a user.',
      });
    }
    await loginValidation.validateAsync(userdIsValid);
    if (await bcrypt.compare(req.body.password, user.password)) {
      const token = jwt.sign({_id: user._id}, config, {expiresIn: 86400});
      res.status(200).send({
        message: 'Success',
        jwt_token: token,
      });
    } else {
      res.status(400).send({
        'message': 'Credentials are not correct.',
      });
    }
  } catch {
    res.status(500).send();
  }
};



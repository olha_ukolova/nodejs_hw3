const db = require('../models');
const Truck = db.truck;
const truckValidation = require('../validation/truck.validation');

exports.createTruck = async (req, res) => {
  const truck = new Truck({
    created_by: req.userId,
    assigned_to: 'nobody',
    status: 'IS',
    type: req.body.type,
  });

  try {
    await truckValidation.validateAsync(truck._doc);
    await truck.save();
    res.send({
      'message': 'Truck created successfully',
    });
  } catch (err) {
    res.status(400).send({
      'message': 'Invalid validation',
    });
  }
};


exports.getTrucks = async (req, res) => {
  try {
    const trucks = await Truck.find();
    if (!trucks.length) {
      res.status(400).send({
        'message': 'There is no trucks',
      });
    } else {
      const trucksList =[];
      for (let i = 0; i<trucks.length; i++) {
        trucksList.push( {
          _id: trucks[i]._id,
          created_by: trucks[i].created_by,
          assigned_to: trucks[i].assigned_to,
          type: trucks[i].type,
          status: trucks[i].status,
          created_date: trucks[i].created_date,
        } );
      }
      res.status(200).send(
          {
            trucks: trucksList,
          },
      );
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};


exports.getTruckById = async (req, res) => {
  try {
    const truck = await Truck.findOne({_id: req.params.id});
    if (!truck) {
      res.status(400).send({
        'message': 'Can`t find truck info',
      });
    } else {
      res.status(200).send(
          {
            truck: {
              _id: truck._id,
              created_by: truck.created_by,
              assigned_to: truck.assigned_to,
              type: truck.type,
              status: truck.status,
              created_date: truck.created_date,
            },
          },
      );
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};


exports.updateTruckById = async (req, res) => {
  try {
    const truck = await Truck.findById({_id: req.params.id});
    if (!truck||truck.assigned_to !=='nobody') {
      res.status(400).send({
        'message': 'There is no truck with such id.',
      });
    } else {
      truck.type = req.body.type;
      await truckValidation.validateAsync(truck._doc);
      await truck.save();
      res.status(200).send({
        'message': 'Truck details changed successfully',
      });
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};


exports.deleteTruckById = async (req, res) => {
  try {
    const truck = await Truck.findOne({_id: req.params.id});
    if (!truck||truck.assigned_to!=='nobody') {
      res.status(400).send({
        'message': 'Can`t find truck info',
      });
    } else {
      const deleted = await Truck.findOneAndDelete({_id: req.params.id});
      if (!deleted) {
        res.status(400).send({
          'message': 'Can`t find truck info!',
        });
      } else {
        res.status(200).send({
          'message': 'Truck deleted successfully',
        });
      }
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};


exports.assignTruckById = async (req, res) => {
  try {
    const truck = await Truck.findOne({assigned_to: req.userId});
    if (!truck) {
      const assigned = await Truck.findOne({_id: req.params.id});
      if (!assigned) {
        res.status(400).send({
          'message': 'There is no truck with such id.',
        });
      } else {
        assigned.assigned_to = req.userId;
        await truckValidation.validateAsync(assigned._doc);
        await assigned.save();
        res.status(200).send({
          'message': 'Truck assigned successfully',
        });
      }
    } else {
      res.status(400).send({
        'message': 'Can`t assign load',
      });
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};

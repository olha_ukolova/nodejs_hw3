const User = require('../models').user;
const bcrypt = require('bcryptjs');


exports.userInfo = async (req, res) => {
  try {
    const user = await User.findById({_id: req.userId});
    if (!user) {
      res.status(400).send({
        'message': 'Can`t find user info',
      });
    } else {
      res.status(200).send({
        user: {
          _id: user._id,
          role: user.role,
          email: user.email,
          created_date: user.created_date,
        },
      });
    }
  } catch (err) {
    res.status(400).send({
      'message': 'Invalid validation',
    });
  }
};

exports.deleteUser = async (req, res) => {
  try {
    const deleted = await User.findByIdAndRemove(req.userId);
    if (!deleted) {
      res.status(400).send({
        'message': 'There is no user with such id.',
      });
    } else {
      res.send({
        'message': 'Profile deleted successfully',
      });
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.changePassword = async (req, res) => {
  try {
    const user = await User.findById({_id: req.userId});
    if (!user) {
      res.status(400).send({
        'message': 'There is no user with such id.',
      });
    } else {
      if (await bcrypt.compare(req.body.oldPassword, user.password)) {
        user.password = bcrypt.hashSync(req.body.newPassword, 8);
        user.save();
        res.status(200).send({
          'message': 'Password changed successfully',
        });
      } else {
        res.status(400).send({
          'message': 'Old password is not correct.',
        });
      }
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

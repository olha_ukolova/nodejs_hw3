const db = require('../models');
const Load = db.load;
const Truck = db.truck;
const User = db.user;
const loadValidation = require('../validation/load.validation');


exports.getLoads = async (req, res) => {
  try {
    const user = await User.findById({_id: req.userId});
    const offset = + (req.query.offset ?? 0);
    const limit = + (req.query.limit ?? 100);
    let filter = null;
    if (req.query.filter) {
      filter = req.query.filter;
    }
    if (!user) {
      res.status(400).send({
        'message': 'Can`t find user info',
      });
    } else {
      let loads;
      if (filter) {
        if (user.role === 'DRIVER') {
          loads = await Load.find({assigned_to: req.userId, status: filter})
              .skip(offset).limit(limit);
        } else {
          loads = await Load.find({created_by: req.userId, status: filter})
              .skip(offset).limit(limit);
        }
      } else {
        if (user.role === 'DRIVER') {
          loads = await Load.find({assigned_to: req.userId})
              .skip(offset).limit(limit);
        } else {
          loads = await Load.find({created_by: req.userId})
              .skip(offset).limit(limit);
        }
      }
      const loadsList =[];
      for (let i = 0; i<loads.length; i++) {
        loadsList.push( {
          _id: loads[i]._id,
          created_by: loads[i].created_by,
          assigned_to: loads[i].assigned_to,
          status: loads[i].status,
          state: loads[i].state,
          name: loads[i].name,
          payload: loads[i].payload,
          pickup_address: loads[i].pickup_address,
          delivery_address: loads[i].delivery_address,
          dimensions: loads[i].dimensions,
          logs: loads[i].logs,
          created_date: loads[i].created_date,
        } );
      }
      res.status(200).send(
          {
            loads: loadsList,
          },
      );
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.createLoad = async (req, res) => {
  try {
    const load = await new Load({
      created_by: req.userId,
      assigned_to: 'nobody',
      status: 'NEW',
      state: '',
      name: req.body.name,
      delivery_address: req.body.delivery_address,
      pickup_address: req.body.pickup_address,
      dimensions: req.body.dimensions,
      payload: req.body.payload,
      logs: [{
        message: 'Load created successfully',
        time: Date.now(),
      }],
    });
    await loadValidation.validateAsync(load._doc);
    await load.save();
    res.send({
      'message': 'Load created successfully',
    });
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.getLoadById = async (req, res) => {
  try {
    const load = await Load.findOne({_id: req.params.id});
    if (!load) {
      res.status(400).send({
        'message': 'Can`t find load info',
      });
    } else {
      res.status(200).send(
          {
            load: {
              _id: load._id,
              created_by: load.created_by,
              assigned_to: load.assigned_to,
              status: load.status,
              state: load.state,
              name: load.name,
              payload: load.payload,
              pickup_address: load.pickup_address,
              delivery_address: load.delivery_address,
              dimensions: load.dimensions,
              logs: load.logs,
              created_date: load.created_date,
            },
          },
      );
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};


exports.updateLoadById = async (req, res) => {
  try {
    const load = await Load.findById({_id: req.params.id});
    if (!load|| load.created_by !== req.userId) {
      res.status(400).send({
        'message': 'There is no load with such id.',
      });
    } else if (load.status === 'NEW') {
      load.name = req.body.name;
      load.delivery_address = req.body.delivery_address;
      load.pickup_address = req.body.pickup_address;
      load.dimensions = req.body.dimensions;
      load.payload = req.body.payload;
    } else {
      res.status(400).send({
        'message': 'There is no new load.',
      });
    }

    await loadValidation.validateAsync(load._doc);
    await load.save();
    res.status(200).send({
      'message': 'Load details changed successfully',
    });
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};


exports.deleteLoadById = async (req, res) => {
  try {
    const load = await Load.findOne({_id: req.params.id});
    if (!load||load.created_by!==req.userId || load.status !== 'NEW') {
      res.status(400).send({
        'message': 'Can`t find truck info',
      });
    } else {
      const deleted = await Load.findOneAndDelete({_id: req.params.id});
      if (!deleted) {
        res.status(400).send({
          'message': 'Can`t find truck info!',
        });
      } else {
        res.status(200).send({
          'message': 'Load deleted successfully',
        });
      }
    }
  } catch (err) {
    res.status(400).send(
        {
          message: err.message,
        },
    );
  }
};


exports.postLoadById = async (req, res, next) => {
  try {
    const load = await Load.findById({_id: req.params.id});
    if (!load) {
      res.status(400).send({
        'message': 'There is no load with such id.',
      });
    } else if (load.status === 'NEW') {
      load.status = 'POSTED';

      await loadValidation.validateAsync(load._doc);
      await load.save();
      req.logs = load.logs;
      next();
    } else {
      res.status(400).send({
        'message': 'Only loads with status \'NEW\' can be posted.',
      });
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.postSystemLoadById = async (req, res, next) => {
  try {
    const load = await Load.findById({_id: req.params.id});
    if (!load) {
      res.status(400).send('The is no posted load.');
    }
    const allTr = await Truck.find({status: 'IS'});
    if (!allTr.length) {
      load.status = 'NEW';
      load.logs = [...req.logs, {
        'message': 'Can`t find driver',
        'time': new Date,
      }];
      await load.save();
      res.status(400).send({
        'message': 'The is no truck in service.',
      });
    } else {
      for (let i=0; i<allTr.length; i++ ) {
        if (allTr === null) {
          return false;
        } else {
          const loadPayload = load.payload;
          const loadWidth = load.dimensions.width;
          const loadLength = load.dimensions.length;
          const loadHeight = load.dimensions.height;

          const truckType = allTr[i].type;

          if (truckType == 'SPRINTER') {
            const truckLength = 300;
            const truckHeight = 250;
            const truckWidth = 170;
            const truckPayload = 1700;

            if (loadWidth < truckWidth && loadLength < truckLength &&
              loadHeight < truckHeight && loadPayload < truckPayload&&
              allTr[i].assigned_to.length > 7) {
              try {
                load.status = 'ASSIGNED';
                load.state = 'En route to Pick Up';
                load.assigned_to = allTr[i].assigned_to;
                const assig = allTr[i].assigned_to;
                load.logs = [...req.logs, {
                  'message': `Load assigned to driver with id ${assig}`,
                  'time': new Date,
                }];
                allTr[i].status = 'OL';
                await allTr[i].save();
                await load.save();
                res.status(200).send({
                  'message': 'Load posted successfully',
                  'driver_found': true,
                });
                break;
              } catch (err) {
                return false;
              }
            }
            if ( (i+1) === allTr.length) {
              load.status = 'NEW';
              load.logs = [...req.logs, {
                'message': 'Can`t find driver',
                'time': new Date,
              }];
              await load.save();
              res.status(400).send({
                'message': 'The is no truck in service.',
              });
            }
          } else if (truckType == 'SMALL STRAIGHT') {
            const truckLength = 500;
            const truckHeight = 250;
            const truckWidth = 170;
            const truckPayload = 2500;

            if (loadWidth < truckWidth && loadLength < truckLength &&
              loadHeight < truckHeight && loadPayload < truckPayload&&
              allTr[i].assigned_to.length > 7) {
              try {
                load.status = 'ASSIGNED';
                load.state = 'En route to Pick Up';
                load.assigned_to = allTr[i].assigned_to;
                const assig = allTr[i].assigned_to;
                load.logs = [...req.logs, {
                  'message': `Load assigned to driver with id ${assig}`,
                  'time': new Date,
                }];
                allTr[i].status = 'OL';
                await allTr[i].save();
                await load.save();
                res.status(200).send({
                  'message': 'Load posted successfully',
                  'driver_found': true,
                });
                break;
              } catch (err) {
                return false;
              }
            }
            if ( (i+1) === allTr.length) {
              load.status = 'NEW';
              load.logs = [...req.logs, {
                'message': 'Can`t find driver',
                'time': new Date,
              }];
              await load.save();
              res.status(400).send({
                'message': 'The is no truck in service.',
              });
            }
          } else if (truckType == 'LARGE STRAIGHT') {
            const truckLength = 700;
            const truckHeight = 350;
            const truckWidth = 200;
            const truckPayload = 4000;

            if (loadWidth < truckWidth && loadLength < truckLength &&
              loadHeight < truckHeight && loadPayload < truckPayload&&
              allTr[i].assigned_to.length > 7) {
              try {
                load.status = 'ASSIGNED';
                load.state = 'En route to Pick Up';
                load.assigned_to = allTr[i].assigned_to;
                const assig = allTr[i].assigned_to;
                load.logs = [...req.logs, {
                  'message': `Load assigned to driver with id ${assig}`,
                  'time': new Date,
                }];
                allTr[i].status = 'OL';
                await allTr[i].save();
                await load.save();
                res.status(200).send({
                  'message': 'Load posted successfully',
                  'driver_found': true,
                });
                break;
              } catch (err) {
                return false;
              }
            }
            if ( (i+1) === allTr.length) {
              load.status = 'NEW';
              load.logs = [...req.logs, {
                'message': 'Can`t find driver',
                'time': new Date,
              }];
              await load.save();
              res.status(400).send({
                'message': 'The is no truck in service.',
              });
            }
          }
        }
      }
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.getActive = async (req, res) => {
  try {
    const loads = await Load.find({assigned_to: req.userId});
    let load;
    for (let i = 0; i<loads.length; i++) {
      if (loads[i].status !== 'SHIPPED') {
        load = loads[i];
        break;
      }
    }
    if (!load) {
      res.status(400).send({
        'message': 'Can`t find active load',
      });
    } else {
      res.status(200).send(
          {
            load: {
              _id: load._id,
              created_by: load.created_by,
              assigned_to: load.assigned_to,
              status: load.status,
              state: load.state,
              name: load.name,
              payload: load.payload,
              pickup_address: load.pickup_address,
              delivery_address: load.delivery_address,
              dimensions: load.dimensions,
              logs: load.logs,
              created_date: load.created_date,
            },
          },
      );
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};

exports.changeState = async (req, res) => {
  try {
    const load = await Load.findOne({assigned_to: req.userId,
      status: 'ASSIGNED'});
    if (!load) {
      res.status(400).send({
        'message': 'There is no load with such id.',
      });
    } else {
      if (load.state === 'En route to Pick Up') {
        load.state = 'Arrived to Pick Up';
      } else if (load.state === 'Arrived to Pick Up') {
        load.state = 'En route to delivery';
      } else if (load.state === 'En route to delivery') {
        load.state = 'Arrived to delivery';
        load.status = 'SHIPPED';
        load.logs = [...load.logs, {
          'message': 'Shipped!',
          'time': new Date,
        }];
        const truck = await Truck.findOne({assigned_to: req.userId});
        truck.status = 'IS';
        await truck.save();
      } else {
        res.status(400).send({
          'message': 'There is no new load.',
        });
      }

      try {
        await load.save();
        res.status(200).send({
          'message': `Load state changed to '${load.state}'`,
        });
      } catch (err) {
        res.status(400).send({
          'message': 'Invalid validation',
        });
      }
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};


exports.shippingInfo = async (req, res) => {
  try {
    const load = await Load.findOne({_id: req.params.id});
    if (!load||load.status !== 'ASSIGNED') {
      res.status(400).send({
        'message': 'Can`t find load info',
      });
    } else {
      const truck = await Truck.findOne({assigned_to: load.assigned_to});
      if (!truck) {
        res.status(400).send({
          'message': 'Can`t find load info!',
        });
      } else {
        res.status(200).send(
            {
              load: {
                _id: load._id,
                created_by: load.created_by,
                assigned_to: load.assigned_to,
                status: load.status,
                state: load.state,
                name: load.name,
                payload: load.payload,
                pickup_address: load.pickup_address,
                delivery_address: load.delivery_address,
                dimensions: load.dimensions,
                logs: load.logs,
                created_date: load.created_date,
              },
              truck: {
                _id: truck._id,
                created_by: truck.created_by,
                assigned_to: truck.assigned_to,
                type: truck.type,
                status: truck.status,
                created_date: truck.created_date,
              },
            },
        );
      }
    }
  } catch (err) {
    res.status(400).send({
      'message': err.message,
    });
  }
};
